# ColorDict Bridge

A bridge app to use Colordict popup using share/dictionary function.

## Rationale
[Colordict](https://play.google.com/store/apps/details?id=com.socialnmobile.colordict&hl=en) has a nice search API ([see here](http://blog.socialnmobile.com/2011/01/colordict-api-for-3rd-party-developers.html)) which only a handful of ebook reader apps has used by far (Actually I've seen only Moon+ Reader do that). While using other reader apps (e.g. Pocketbook), I can use Colordict as the dictionary but it loads the full window instead of pop-up. This app is to mitigate that problem. Instead of choosing **Colordict**, choose **ColorDict Bridge** as the dictionary app and you're good to go.
