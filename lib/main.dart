import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:io';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';

import 'package:android_intent/android_intent.dart';
// import 'package:intent/intent.dart' as android_intent;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  StreamSubscription _intentDataStreamSubscription;
  String _sharedText;

  @override
  void initState() {
    super.initState();

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
      setState(() {
        _sharedText = value;
      });
    }, onError: (err) {
      print("getLinkStream error: $err");
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String value) {
      setState(() {
        _sharedText = value;
      });
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const textStyleBold = const TextStyle(fontWeight: FontWeight.bold);
    return MaterialApp(home: HomePage(_sharedText));
  }
}

class HomePage extends StatelessWidget {
  final String term;
  HomePage(this.term);

  void sendToColorDict(context) {
    var intent = AndroidIntent(
      action: 'colordict.intent.action.SEARCH',
      arguments: <String, dynamic>{
        'EXTRA_QUERY': term,
        'EXTRA_FULLSCREEN': false,
        'EXTRA_HEIGHT': (MediaQuery.of(context).size.height *
                MediaQuery.of(context).devicePixelRatio *
                0.40)
            .round(),
        'EXTRA_GRAVITY': 16
      },
    );
    intent.launch().then((value) => exit(0));
    // exit(0);
  }

  @override
  Widget build(BuildContext context) {
    if (term != null) {
      sendToColorDict(context);
      return Opacity(opacity: 0, child: Text(":p"));
    } else {
      return Scaffold(
          appBar: AppBar(
            title: const Text('ColorDict Bridge'),
          ),
          body: Column(children: <Widget>[
            Center(
              child: Text(
                  "This app allow you to use ColorDict as Pop-up for unsupported apps. e.g. PocketBook Reader. Instead of selecting Pocketbook as dictionary app, select this App."),
            )
          ]));
    }
  }
}
